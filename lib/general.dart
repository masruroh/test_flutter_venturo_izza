import 'package:flutter/material.dart';

Color tema = Color(0XFF56B8CA), tema2 = Color(0XFF363563), putih = Colors.white;
double rad = 5, jarak = 10, elev = 10;

style(double size,
    {Color color = Colors.black, bool i = false, bool b = false}) {
  return TextStyle(
      color: color,
      fontSize: size,
      fontStyle: (i) ? FontStyle.italic : FontStyle.normal,
      fontWeight: (b) ? FontWeight.bold : FontWeight.normal);
}

// UNTUK LOADING
var load = Center(
  child: Container(
    width: 150,
    child: Image.asset('assets/loading.gif', fit: BoxFit.cover),
  ),
);
loading() {
  return SafeArea(child: Scaffold(body: load));
}

alert(
  BuildContext context, {
  Widget content,
}) {
  return showDialog(
    context: context,
    builder: (BuildContext context) {
      return AlertDialog(
        // title: Center(child: Text(title)),
        // actions: [MaterialButton(child: Text('OK'), onPressed: ok)],
        content: content,
        // child: ListBody(children: [Text(title)]),
      );
    },
  );
}

notif(BuildContext context, {String title, String content, Function ok}) {
  return showDialog(
    context: context,
    builder: (BuildContext context) {
      return AlertDialog(
        title: Center(child: Text(title)),
        actions: [MaterialButton(child: Text('OK'), onPressed: ok)],
        content: SingleChildScrollView(child: Text(content)),
        // child: ListBody(children: [Text(title)]),
      );
    },
  );
}

// -------------------- CLASS DATA
class SubMenu {
  SubMenu(
      {this.id,
      this.nourut,
      this.nama,
      this.level,
      this.parentid,
      this.pelajaranid,
      this.isdeleted,
      this.createdat,
      this.createdby,
      this.modifiedat,
      this.modifiedby,
      this.detail});
  int id,
      nourut,
      level,
      parentid,
      pelajaranid,
      isdeleted,
      createdat,
      createdby,
      modifiedat,
      modifiedby;
  String nama;
  List<DetailSubMenu> detail;
}

class DetailSubMenu {
  DetailSubMenu({this.youtubeid, this.youtubeurl});
  String youtubeid, youtubeurl;
}

// class untuk lanjutan
class User {
  User(
      {this.id,
      this.nama,
      this.email,
      this.alamat,
      this.telepon,
      this.username,
      this.password,
      this.role,
      this.isdeleted,
      this.hakakses});
  String id,
      nama,
      email,
      alamat,
      telepon,
      username,
      password,
      role,
      isdeleted,
      hakakses;
}
