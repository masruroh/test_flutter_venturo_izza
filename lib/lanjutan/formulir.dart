import 'dart:convert';

import 'package:flutter/material.dart';

import '../general.dart';
import 'package:http/http.dart' as http;

class FormulirPage extends StatefulWidget {
  const FormulirPage({this.data});
  final User data;
  @override
  _FormulirPageState createState() => _FormulirPageState();
}

class _FormulirPageState extends State<FormulirPage> {
  bool get = true;
  String judul;
  var tombolAksi;
  String namaS, emailS, alamatS, teleponS, usernameS, passwordS = '', hakaksesS;
  TextEditingController namaTEC,
      emailTEC,
      alamatTEC,
      teleponTEC,
      usernameTEC,
      passwordTEC,
      hakaksesTEC;
  addData({bool deleted = false}) async {
    setState(() => get = false);
    String url = 'https://smkyp17pare.sch.id/app/api/site/saveUser';

    var body = {
      'nama': namaS,
      'email': emailS,
      'alamat': alamatS,
      'telepon': teleponS,
      'username': usernameS,
      'm_roles_id': '1',
      'is_deleted': (deleted) ? '1' : '0',
      'hakakses': 'Super Admin',
    };
    if (widget.data != null) {
      body['id'] = widget.data.id;
      body['password'] = '';
    } else {
      body['password'] = passwordS;
    }
    print(body);
    // setState(() => get = true);
    await http.post(Uri.parse(url), body: body).then(
      (http.Response response) {
        setState(() => get = true);
        var temp = jsonDecode(response.body);
        print(temp);
        if (temp['status_code'] == 200) {
          notif(context, title: 'Sukses', content: 'Proses berhasil dilakukan',
              ok: () {
            Navigator.of(context).pop();
            Navigator.of(context).pop();
          });
        }
        // getData();
      },
    );
  }

  field(String title, Function onchange,
      {TextEditingController tec,
      bool num = false,
      Color color,
      bool b = false,
      String hint = '',
      bool focus = false,
      bool star = false}) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(title, style: style(17, b: b)),
        Container(
          padding: EdgeInsets.fromLTRB(jarak, 0, jarak, 0),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(rad),
            border: Border.all(),
            color: (color == null) ? putih : color,
          ),
          child: TextFormField(
            controller: tec,
            onChanged: onchange,
            obscureText: star,
            keyboardType: (num) ? TextInputType.number : TextInputType.text,
            decoration:
                InputDecoration(border: InputBorder.none, hintText: hint),
            autofocus: focus,
          ),
        ),
        SizedBox(height: jarak),
      ],
    );
  }

  fieldAuto(String title, String value, {int maxLine = 4}) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(title, style: style(17)),
        Container(
            padding: EdgeInsets.fromLTRB(jarak, 16, jarak, 16),
            width: double.infinity,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(rad),
                border: Border.all(),
                color: Colors.grey[300]),
            // color: abu),
            child: Text(
              value,
              style: style(17),
              maxLines: maxLine,
              overflow: TextOverflow.ellipsis,
            )),
      ],
    );
  }

  _decide() {
    return SafeArea(
        child: Scaffold(
      appBar: AppBar(title: Text(judul), actions: [
        (widget.data == null)
            ? Container()
            : IconButton(
                icon: Icon(Icons.delete),
                onPressed: () => addData(deleted: true))
      ]),
      backgroundColor: Colors.blue[100],
      body: SingleChildScrollView(
        child: Container(
            padding: EdgeInsets.all(jarak),
            child: Column(
              children: [
                field('Nama', (value) {
                  setState(() => namaS = value);
                }, tec: namaTEC, hint: 'Masukkan Nama'),
                field('Email', (value) {
                  setState(() => emailS = value);
                }, tec: emailTEC, hint: 'Masukkan Email'),
                field('Alamat', (value) {
                  setState(() => alamatS = value);
                }, tec: alamatTEC, hint: 'Masukkan Alamat'),
                field('Telepon', (value) {
                  setState(() => teleponS = value);
                }, tec: teleponTEC, hint: 'Masukkan Telepon', num: true),
                field('Username', (value) {
                  setState(() => usernameS = value);
                }, tec: usernameTEC, hint: 'Masukkan Username'),
                tombolAksi,
              ],
            )),
      ),
    ));
  }

  @override
  void initState() {
    super.initState();
    setState(() {
      if (widget.data == null) {
        judul = 'Tambah Data';
        tombolAksi = Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            field('Password', (value) {
              setState(() {
                passwordS = value;
              });
            }, tec: passwordTEC, hint: 'Masukkan Password', star: true),
            // Text('Password', style: style(17)),
            // Container(
            //   padding: EdgeInsets.only(left: jarak, right: jarak),
            //   decoration: BoxDecoration(
            //       borderRadius: BorderRadius.circular(rad),
            //       border: Border.all(),
            //       color: putih),
            //   child: TextField(
            //     controller: passwordTEC,
            //     obscureText: _obscureText,
            //     decoration: InputDecoration(
            //       hintText: "Masukkan Password",
            //       border: InputBorder.none,
            //       suffixIcon: GestureDetector(
            //           child: Icon(
            //             (_obscureText)
            //                 ? Icons.visibility
            //                 : Icons.visibility_off,
            //             // color: Colors.black.withOpacity(0.5),
            //           ),
            //           onTap: () {
            //             setState(() => _obscureText = !_obscureText);
            //           }),
            //     ),
            //     onChanged: (value) {
            //       setState(() => passwordS = value);
            //     },
            //   ),
            // ),
            SizedBox(height: jarak),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                MaterialButton(
                    child: Text('Send'),
                    onPressed: () => addData(),
                    color: Colors.blue),
              ],
            )
          ],
        );
      } else {
        judul = 'Edit Data';
        tombolAksi = Column(
          children: [
            fieldAuto('Roles', widget.data.role),
            SizedBox(height: jarak),
            fieldAuto('Hak Akses', widget.data.hakakses),
            SizedBox(height: jarak),
            MaterialButton(
                child: Text('Update'),
                onPressed: () => addData(),
                color: Colors.blue),
          ],
        );
        namaS = widget.data.nama;
        namaTEC = TextEditingController(text: namaS);

        emailS = widget.data.email;
        emailTEC = TextEditingController(text: emailS);

        alamatS = widget.data.alamat;
        alamatTEC = TextEditingController(text: alamatS);

        teleponS = widget.data.telepon;
        teleponTEC = TextEditingController(text: teleponS);

        usernameS = widget.data.username;
        usernameTEC = TextEditingController(text: usernameS);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return (get) ? _decide() : loading();
  }
}
