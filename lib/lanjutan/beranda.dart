import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import '../general.dart';
import 'formulir.dart';

class BerandaPage extends StatefulWidget {
  @override
  _BerandaPageState createState() => _BerandaPageState();
}

class _BerandaPageState extends State<BerandaPage> {
  bool get = true;
  List<User> listData;
  getData() async {
    print('proses geData');
    setState(() => listData = []);
    setState(() => get = false);

    await http
        .get(Uri.parse('https://smkyp17pare.sch.id/app/api/site/listUser'))
        .then(
      (http.Response response) {
        setState(() => get = true);
        // FUNGSI
        var temp = jsonDecode(response.body);
        // print(temp);
        // berhasil
        if (temp['status_code'] == 200) {
          setListData(temp['data']['list']); // update listdata
          // print(temp['data']['list']);
        }
        // gagal kirim
        else {
          print('gagal post');
          // Navigator.of(context).pop();
        }
      },
    );
  }

  setListData(List data) {
    for (var i = 0; i < data.length; i++) {
      listData.add(User(
        id: (data[i]['id'] == null) ? '' : data[i]['id'],
        nama: (data[i]['nama'] == null) ? '' : data[i]['nama'],
        email: (data[i]['email'] == null) ? '' : data[i]['email'],
        alamat: (data[i]['alamat'] == null) ? '' : data[i]['alamat'],
        telepon: (data[i]['telepon'] == null) ? '' : data[i]['telepon'],
        username: (data[i]['username'] == null) ? '' : data[i]['username'],
        role: (data[i]['m_roles_id'] == null) ? '' : data[i]['m_roles_id'],
        isdeleted: (data[i]['is_deleted'] == null) ? '' : data[i]['is_deleted'],
        hakakses: (data[i]['hakakses'] == null) ? '' : data[i]['hakakses'],
      ));
    }
  }

  detailData(User data) {
    return showDialog(
      context: context,
      builder: (context) {
        return StatefulBuilder(
          builder: (context, setState) {
            return AlertDialog(
              title: Text('Detail Data', style: style(20, b: true)),
              content: Form(
                  child: SingleChildScrollView(
                child: Column(
                  children: [
                    Row(children: [
                      Container(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text('Nama'),
                            Text('Email'),
                            Text('Alamat'),
                            Text('Telepon'),
                            Text('Username'),
                            Text('Roles'),
                            Text('Hak Akses'),
                          ],
                        ),
                      ),
                      Flexible(
                        child: Container(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(' : ' + data.nama,
                                  overflow: TextOverflow.ellipsis),
                              Text(' : ' + data.email,
                                  overflow: TextOverflow.ellipsis),
                              Text(' : ' + data.alamat,
                                  overflow: TextOverflow.ellipsis),
                              Text(' : ' + data.telepon,
                                  overflow: TextOverflow.ellipsis),
                              Text(' : ' + data.username,
                                  overflow: TextOverflow.ellipsis),
                              Text(' : ' + data.role,
                                  overflow: TextOverflow.ellipsis),
                              Text(' : ' + data.hakakses,
                                  overflow: TextOverflow.ellipsis),
                            ],
                          ),
                        ),
                      ),
                    ]),
                    SizedBox(height: jarak),
                    Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                      MaterialButton(
                          onPressed: () => Navigator.of(context).pop(),
                          child: Text('Cancel'),
                          color: Colors.yellowAccent),
                      SizedBox(width: jarak),
                      MaterialButton(
                          onPressed: () {
                            Navigator.of(context).pop();
                            Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) =>
                                            FormulirPage(data: data)))
                                .then(_goBack);
                          },
                          child: Text('Edit'),
                          color: Colors.blue),
                    ]),
                  ],
                ),
              )),
            );
          },
        );
      },
    );
  }

  contData(User data) {
    return Container(
      margin: EdgeInsets.all(jarak / 2),
      child: GestureDetector(
        onTap: () => detailData(data),
        child: Material(
          elevation: elev,
          borderRadius: BorderRadius.circular(rad),
          color: Colors.blue[100],
          child: Container(
            padding: EdgeInsets.all(jarak),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(rad),
                color: Colors.blue[100]),
            child: Row(children: [
              Image.asset('assets/man1.png', width: 25, height: 25),
              SizedBox(width: jarak),
              Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
                Text(data.nama, style: style(19, b: true)),
                Text(data.email, style: style(17))
              ]),
            ]),
          ),
        ),
      ),
    );
  }

  FutureOr _goBack(dynamic value) {
    getData();
  }

  _decide() {
    return SafeArea(
      child: Scaffold(
          appBar: AppBar(title: Text('List User')),
          floatingActionButton: FloatingActionButton(
            child: Icon(Icons.add),
            onPressed: () {
              Navigator.push(context,
                      MaterialPageRoute(builder: (context) => FormulirPage()))
                  .then(_goBack);
            },
          ),
          body: Container(
            padding: EdgeInsets.all(jarak),
            child: ListView.builder(
                itemCount: listData.length,
                itemBuilder: (context, i) {
                  return contData(listData[i]);
                }),
          )),
    );
  }

  @override
  void initState() {
    super.initState();
    setState(() => listData = []);
    getData();
  }

  @override
  Widget build(BuildContext context) {
    return (get) ? _decide() : loading();
  }
}
