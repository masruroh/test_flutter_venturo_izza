import 'package:flutter/material.dart';
import 'package:youtube_player_flutter/youtube_player_flutter.dart';

import '../general.dart';

class VideoExample extends StatefulWidget {
  //
  // VideoExample({this.videoItem});
  // final VideoItem videoItem;
  const VideoExample({this.data});
  final DetailSubMenu data;
  @override
  _VideoExampleState createState() => _VideoExampleState();
}

class _VideoExampleState extends State<VideoExample> {
  //
  YoutubePlayerController _controller;
  bool _isPlayerReady;
  @override
  void initState() {
    super.initState();
    _isPlayerReady = false;
    _controller = YoutubePlayerController(
      initialVideoId: widget.data.youtubeid,
      flags: YoutubePlayerFlags(
        mute: false,
        autoPlay: true,
      ),
    )..addListener(_listener);
  }

  void _listener() {
    if (_isPlayerReady && mounted && !_controller.value.isFullScreen) {
      //
    }
  }

  @override
  void deactivate() {
    _controller.pause();
    super.deactivate();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: YoutubePlayer(
        controller: _controller,
        showVideoProgressIndicator: true,
        onReady: () {
          print('Player is ready.');
          _isPlayerReady = true;
        },
      ),
    );
  }
}

class VideoExampleDisplay extends StatefulWidget {
  //
  // VideoExampleDisplay({this.videoItem});
  // final VideoItem videoItem;
  const VideoExampleDisplay({this.data});
  final DetailSubMenu data;
  @override
  _VideoExampleDisplayState createState() => _VideoExampleDisplayState();
}

class _VideoExampleDisplayState extends State<VideoExampleDisplay> {
  //
  YoutubePlayerController _controller;
  bool _isPlayerReady;
  @override
  void initState() {
    super.initState();
    _isPlayerReady = false;
    _controller = YoutubePlayerController(
      initialVideoId: widget.data.youtubeid,
      flags: YoutubePlayerFlags(
        mute: false,
        hideControls: true,
        controlsVisibleAtStart: true,
        autoPlay: false,
      ),
    )..addListener(_listener);
  }

  void _listener() {
    if (_isPlayerReady && mounted && !_controller.value.isFullScreen) {
      //
    }
  }

  @override
  void deactivate() {
    _controller.pause();
    super.deactivate();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: YoutubePlayer(
        controller: _controller,
        showVideoProgressIndicator: true,
        onReady: () {
          print('Player is ready.');
          _isPlayerReady = true;
        },
      ),
    );
  }
}
