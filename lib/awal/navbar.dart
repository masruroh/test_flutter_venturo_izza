import 'package:flutter/material.dart';

import '../general.dart';

class NavBar extends StatelessWidget {
  contProfil() {
    return Container(
        child: Row(
      children: [
        Container(
          padding: EdgeInsets.all(jarak),
          decoration: BoxDecoration(shape: BoxShape.circle, color: putih),
          child: Image.asset(
            'assets/man1.png',
            fit: BoxFit.fill,
            width: 50,
            height: 50,
          ),
        ),
        SizedBox(width: jarak),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text('Dhena Aprilia', style: style(17, color: putih, b: true)),
            Text('45233', style: style(15, color: putih)),
            Text('Dhena Aprilia', style: style(15, color: putih)),
          ],
        ),
      ],
    ));
  }

  contData(Icon icon, String menu, {Icon trailing}) {
    return Container(
      margin: EdgeInsets.only(bottom: jarak * 3),
      child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
        Row(children: [
          icon,
          SizedBox(width: jarak),
          Text(menu, style: style(15, color: putih))
        ]),
        (trailing == null) ? Container() : trailing,
      ]),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Container(
        padding:
            EdgeInsets.fromLTRB(jarak * 2, jarak * 3, jarak * 2, jarak * 2),
        color: tema2,
        child: ListView(
          children: [
            contProfil(),
            SizedBox(height: jarak * 2),
            contData(Icon(Icons.home, color: putih), 'Beranda'),
            contData(Icon(Icons.computer, color: putih), 'Ujian'),
            contData(Icon(Icons.check_box, color: putih), 'Hasil Ujian'),
            contData(Icon(Icons.notifications, color: putih), 'Notifikasi'),
            contData(Icon(Icons.library_books, color: putih),
                'Perpustakaan Digital'),
            contData(Icon(Icons.person, color: putih), 'Profile',
                trailing: Icon(Icons.keyboard_arrow_down)),
            contData(Icon(Icons.exit_to_app, color: putih), 'Keluar'),
          ],
        ),
      ),
    );
  }
}
