import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:video_player/video_player.dart';

import '../general.dart';
import 'package:http/http.dart' as http;

import 'navbar.dart';
import 'testvideo.dart';

class BerandaPage extends StatefulWidget {
  @override
  _BerandaPageState createState() => _BerandaPageState();
}

class _BerandaPageState extends State<BerandaPage> {
  List<SubMenu> listData = [];
  bool get = true;
  VideoPlayerController playerController;
  VoidCallback listener;
  getData() async {
    print('proses geData');
    setState(() => get = false);

    await http
        .get(Uri.parse('https://tes-mobile.landa.id/index.php')
            // headers: header
            )
        .then(
      (http.Response response) {
        setState(() => get = true);
        // FUNGSI
        var temp = jsonDecode(response.body);
        print(temp);
        // berhasil
        if (temp['status_code'] == 200) {
          setListData(temp['data']); // update listdata
        }
        // gagal kirim
        else {
          print('gagal post');
          // Navigator.of(context).pop();
        }
      },
    );
  }

  setListData(List data) {
    setState(() => listData = []);
    for (var i = 0; i < data.length; i++) {
      SubMenu submenu = SubMenu(
        id: data[i]['id'],
        nourut: data[i]['nourut'],
        nama: data[i]['nama'],
        level: data[i]['level'],
        parentid: data[i]['parent_id'],
        pelajaranid: data[i]['pelajaran_id'],
        isdeleted: data[i]['isdeleted'],
        createdat: data[i]['created_at'],
        createdby: data[i]['created_by'],
        modifiedat: data[i]['modified_at'],
        modifiedby: data[i]['modified_by'],
      );
      List<DetailSubMenu> tempDetail = [];
      var detail = data[i]['detail'];
      for (var j = 0; j < detail.length; j++) {
        var temp = data[i]['detail'][j];
        tempDetail.add(DetailSubMenu(
            youtubeid: temp['youtube_id'], youtubeurl: temp['youtube_url']));
      }
      submenu.detail = tempDetail;
      listData.add(submenu);
    }
  }

  contData(SubMenu data) {
    return Container(
      width: double.infinity,
      child: Column(
        children: [
          Container(
            width: double.infinity,
            padding: EdgeInsets.all(jarak),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(rad * 2),
              color: tema,
            ),
            child: Text(data.nama),
          ),

          SizedBox(height: jarak),

          // video
          Container(
              color: tema,
              child: GestureDetector(
                onTap: () {
                  alert(context, content: VideoExample(data: data.detail[0]));
                  // Navigator.push(
                  //     context,
                  //     MaterialPageRoute(
                  //         builder: (context) => VideoExample(data: data.detail[0])));
                },
                child: VideoExampleDisplay(
                  data: data.detail[0],
                ),
                // child: Text('TESTING VIDEO')
              )),

          SizedBox(height: jarak),
        ],
      ),
    );
  }

  _decide() {
    return SafeArea(
        child: Scaffold(
      drawer: NavBar(),
      body: Column(children: [
        Stack(children: [
          Image.network(
            'https://tes-mobile.landa.id/top_home.png',
            height: MediaQuery.of(context).size.height / 2.5,
            width: double.infinity,
            fit: BoxFit.fill,
          ),
          Container(
            padding: EdgeInsets.all(jarak),
            child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Icon(Icons.menu, color: putih),
                  Text('Materi Pembelajaran', style: style(15, color: putih)),
                  Icon(Icons.notifications, color: putih),
                ]),
          ),
        ]),
        Flexible(
          child: Container(
            padding: EdgeInsets.all(jarak),
            child: ListView.builder(
              itemBuilder: (context, i) {
                return contData(listData[i]);
              },
              itemCount: listData.length,
            ),
          ),
        )
      ]),
    ));
  }

  createVideo(DetailSubMenu data) {
    if (playerController == null) {
      playerController = VideoPlayerController.network(data.youtubeurl)
        ..addListener(listener)
        ..setVolume(1.0)
        ..initialize()
        ..play();
    } else {
      if (playerController.value.isPlaying) {
        playerController.pause();
      } else {
        playerController.initialize();
        playerController.play();
      }
    }
  }

  @override
  void initState() {
    super.initState();
    getData();
    listener = () {
      setState(() {});
    };
  }

  @override
  Widget build(BuildContext context) {
    return (get) ? _decide() : loading();
  }
}
